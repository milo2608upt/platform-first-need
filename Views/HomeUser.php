<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Delivery - HomeUser</title>
    <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.5.0/united/bootstrap.min.css" rel="stylesheet" integrity="sha384-Uga2yStKRHUWCS7ORqIZhJ9LIAv4i7gZuEdoR1QAmw6H+ffhcf7yCOd0CvSoNwoz" crossorigin="anonymous">
    <link rel="stylesheet" href="../../PlataformaFirstNeed/css/ChatStyle.css">
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
      <a class="navbar-brand" href="?controller=usuario&action=homeUser">
        <img src="../../PlataformaFirstNeed/Images/Delivery_Logo.png" width="30" height="30" alt="" loading="lazy">
        DeliveryFR
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
          <li class="nav-item active" style="width: 88.3px; margin-top: 1px; margin-right: 0;">
            <a class="nav-link" style="width: 90px" href="#"><i class="fas fa-home"></i> Inicio</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#"><i class="fas fa-truck"></i> Pedidos</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="?controller=producto&action=see"><i class="fas fa-box-open"></i> Productos</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#"><span data-toggle="tooltip" title="Haz Tomado: 10 Productos"><i class="fas fa-shopping-cart"></i> Carrito</span></a>
          </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
          <input class="form-control mr-sm-2" type="search" placeholder="Search">
          <button class="btn btn-outline-dark my-2 my-sm-0" type="submit">Search</button>
        </form>
      </div>
    </nav>
    <div class="container">
      <div class="msg-header">
        <div class="msg-header-img">
          <img src="../../PlataformaFirstNeed/Images/msg-chat.jpg" class="img-chat">
        </div>
        <div class="active" id="state">
          <h4>Pedido</h4>
        </div>
      </div>
      <div class="chat-page">
        <div class="msg-inbox">
          <div class="chat">
            <div class="msg-page" id="chat">
              <div class="received-chats">
                <div class="received-chats-img">
                  <img src="../../PlataformaFirstNeed/Images/User.jpg" class="img-chat-customer">
                </div>
                <div class="received-msg">
                  <div class="received-msg-inbox">
                    <p>Hi This is A message from Customer</p>
                    <span class="time">11:01 PM | June 20</span>
                  </div>
                </div>
              </div>

              <div class="out-chats">
                <div class="out-chats-msg">
                  <p>Hi !! This is message from me</p>
                  <span class="time">11:01 PM | June 20</span>
                </div>
                <div class="out-chats-img">
                  <img src="../../PlataformaFirstNeed/Images/response.jpg" class="img-chat-response">
                </div>
              </div>

              <div class="received-chats">
                <div class="received-msg">
                  <div class="received-msg-inbox">
                    <p>Hi This is A message from Customer</p>
                    <span class="time">11:01 PM | June 20</span>
                  </div>
                </div>
                <div class="received-chats-img">
                  <img src="../../PlataformaFirstNeed/Images/User.jpg" class="img-chat-customer">
                </div>
              </div>

              <div class="out-chats">
                <div class="out-chats-msg">
                  <p>Hi !! This is message from me</p>
                  <span class="time">11:01 PM | June 20</span>
                </div>
                <div class="out-chats-img">
                  <img src="../../PlataformaFirstNeed/Images/response.jpg" class="img-chat-response">
                </div>
              </div>

              <div class="received-chats">
                <div class="received-msg">
                  <div class="received-msg-inbox">
                    <p>Hi This is A message from Customer</p>
                    <span class="time">11:01 PM | June 20</span>
                  </div>
                </div>
                <div class="received-chats-img">
                  <img src="../../PlataformaFirstNeed/Images/User.jpg" class="img-chat-customer">
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>

      <div class="msg-bottom">
        <div class="bottom-icons">
          <i class="fas fa-microphone"></i>
          <i class="fas fa-comment-alt"></i>
        </div>
        <div class="input-group">
          <input type="text" name="" class="form-control" readonly placeholder="Presiona el boton mesaje...">
          <div class="input-group-append">
            <span class="input-group-text">
              <i class="fa fa-paper-plane"></i>
            </span>
          </div>
        </div>
      </div>
    </div>
    <script src="https://kit.fontawesome.com/4c3daebd76.js" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/annyang/2.6.0/annyang.min.js"></script>
    <script src="../../PlataformaFirstNeed/js/voice.js"></script>
  </body>
</html>
