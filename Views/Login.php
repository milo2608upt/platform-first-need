<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Delivery - LogIn</title>
    <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.5.0/united/bootstrap.min.css" rel="stylesheet" integrity="sha384-Uga2yStKRHUWCS7ORqIZhJ9LIAv4i7gZuEdoR1QAmw6H+ffhcf7yCOd0CvSoNwoz" crossorigin="anonymous">
  </head>
  <style media="screen">
    img{
      height: 600px !important;
    }
  </style>
  <body class="bg-primary">

    <div class="container">
      </div>
        <div class="col-lg-4 offset-lg-4">
          <?php

          if(isset($_SESSION['status'])){
            echo '<p class="h6 alert alert-danger mt-1">'.$_SESSION["status"].'</p>';
          }
          unset($_SESSION['status']);
          if(isset($_SESSION['flash'])){
              echo '<p class="h6 alert alert-success mt-1">'.$_SESSION["flash"].'</p>';
          }
          unset($_SESSION['flash']);
          ?>
          <div class=" shadow-lg card mt-3">
            <div class="card-body">
              <div class="row">
                <div class="col-lg-12">
                  <h3 class="d-flex justify-content-center">LogIn</h3>
                  <p class="d-flex justify-content-center italic">Delivery of First Need</p>
                </div>
                <div class="col-lg-12">
                  <img src="../Images/Delivery_Login.jpg" class="" alt="" style="height: 150px !important;">
                </div>
              </div>
              <form class="" action="?controller=usuario&action=validation" method="post">
                <div class="form-group">
                  <label for="">Email:</label>
                  <input type="text" name="username" class="form-control" placeholder="Correo Electronico">
                </div>
                <div class="form-group">
                  <label for="">Password:</label>
                  <input type="password" name="password" class="form-control" placeholder="Password">
                </div>
                <button type="submit" class="btn btn-primary btn-lg btn-block" name="button">INGRESAR</button>
                <a href="?controller=usuario&action=registry" class="btn btn-outline-secondary btn-lg btn-block">Registrate</a>
                <a href="#"class="d-flex justify-content-center">¿Haz olvidado tu contraseña?</a>
              </form>
            </div>
          </div>
        </div>
      </div>
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
  <script src="https://kit.fontawesome.com/4c3daebd76.js" crossorigin="anonymous"></script>
  </body>
</html>
