<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Delivery - LogIn</title>
    <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.5.0/united/bootstrap.min.css" rel="stylesheet" integrity="sha384-Uga2yStKRHUWCS7ORqIZhJ9LIAv4i7gZuEdoR1QAmw6H+ffhcf7yCOd0CvSoNwoz" crossorigin="anonymous">
  </head>
  <style media="screen">
    img{
      height: 600px !important;
    }
  </style>
  <body class="bg-primary">

    <div class="container">
      </div>
        <div class="col-lg-4 offset-lg-4">
          <?php

          if(isset($_SESSION['status'])){
            echo '<p class="h6 alert alert-danger mt-1">'.$_SESSION["status"].'</p>';
          }
          unset($_SESSION['status']);
          ?>
          <div class=" shadow-lg card mt-2">
            <div class="card-body">
              <a class="" href="?controller=usuario&action=login"><i class="fas fa-chevron-left"></i> Back</a>
              <div class="row">
                <div class="col-6 col-lg-6 col-sm-6">
                  <h4 class="d-flex justify-content-center mt-4">SignUp</h4>
                  <p class="d-flex justify-content-center italic">Delivery of First Need</p>
                </div>
                <div class="col-6 col-lg-6 col-sm-6">
                  <img src="../Images/Delivery_Login.jpg" class="" alt="" style="height: 109px !important;">
                </div>
              </div>
              <form class="" action="?controller=usuario&action=signup" method="post">
                <div class="row">
                  <div class="col-6">
                    <div class="form-group my-2">
                      <label for="">Nombre:</label>
                        <input type="text" name="nombre" class="form-control" placeholder="Nombre">
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="form-group my-2">
                      <label for="">Apellidos:</label>
                      <input type="text" name="apellidos" class="form-control" placeholder="Apellidos">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-4">
                    <div class="form-group my-2">
                      <label for="">Dia:</label>
                      <select class="custom-select" name="dia" id="inputGroupSelect01">
                      <?php
                        for ($i=1; $i < 32; $i++) {
                          if($i==1){
                            echo "<option value='".$i."' selected>".$i."</option>";
                          }else{
                            echo "<option value='".$i."'>".$i."</option>";
                          }
                        }
                      ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group my-2">
                      <label for="">Mes:</label>
                      <select class="custom-select" name="mes" id="inputGroupSelect01" placeholder="Mes">
                      <?php
                        $meses = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Nobiembre","Diciembre"];
                        for ($i=0; $i < 12 ; $i++) {
                          echo "<option value='".$i."'>".$meses[$i]."</option>";
                        }
                      ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-4">
                    <div class="form-group my-2">
                      <label for="">Año:</label>
                      <select class="custom-select" name="año" id="inputGroupSelect01">
                      <?php
                        for ($i=2020; $i > 1904; $i--) {
                          if($i==2020){
                            echo "<option value='".$i."' selected>".$i."</option>";
                          }else{
                            echo "<option value='".$i."'>".$i."</option>";
                          }
                        }
                      ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form-group my-2">
                  <label for="">Email:</label>
                  <input type="text" name="email" class="form-control" placeholder="Correo Electronico">
                </div>
                <div class="form-group my-2">
                  <label for="">Password:</label>
                  <input type="text" name="password" class="form-control" placeholder="Password">
                </div>
                <div class="form-group my-2">
                  <label for="">Tipo de Usuario:</label>
                  <select required class="custom-select" name="tipo" id="inputGroupSelect01">
                    <option value="0"selected>Choose...</option>
                    <option value="1">Cliente</option>
                    <option value="2">Administrador</option>
                    <option value="3">DeliveryFN</option>
                  </select>
                </div>
                <button type="submit" class="btn btn-primary btn-lg btn-block mb-0" name="button">REGISTRATE</button>
              </form>
            </div>
          </div>
        </div>
      </div>
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
  <script src="https://kit.fontawesome.com/4c3daebd76.js" crossorigin="anonymous"></script>
  </body>
</html>
