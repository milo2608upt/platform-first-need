<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Delivery - Products</title>
    <link href="https://stackpath.bootstrapcdn.com/bootswatch/4.5.0/united/bootstrap.min.css" rel="stylesheet" integrity="sha384-Uga2yStKRHUWCS7ORqIZhJ9LIAv4i7gZuEdoR1QAmw6H+ffhcf7yCOd0CvSoNwoz" crossorigin="anonymous">
    <link rel="stylesheet" href="../../PlataformaFirstNeed/css/Style.css">
  </head>
  <body>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <a class="navbar-brand" href="?controller=usuario&action=homeUser">
        <img src="../../PlataformaFirstNeed/Images/Delivery_Logo.png" width="30" height="30" alt="" loading="lazy">
        DeliveryFR
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
          <li class="nav-item">
            <a class="nav-link" href="?controller=usuario&action=homeUser"><i class="fas fa-home"></i> Inicio</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#"><i class="fas fa-truck"></i> Pedidos</a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="?controller=producto&action=see"><i class="fas fa-box-open"></i> Productos</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#"><span data-toggle="tooltip" title="Haz Tomado: 10 Productos"><i class="fas fa-shopping-cart"></i> Carrito</span></a>
          </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
          <input class="form-control mr-sm-2" type="search" placeholder="Search">
          <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>
      </div>
    </nav>
    <div class="py-2 my-4 container shadow-lg bg-primary">
      <div class="row">
          <?php

          $desc="";
          $descComp="";
            if(isset($_SESSION['productos'])){
              foreach ($_SESSION['productos'] as $producto) {
                //echo $producto[1];
                /*$arreglo=explode(" ",$producto[2]);
                for($i=0; $i<20; $i++){
                  $desc=$desc." ".$arreglo[$i];
                }
                for($j=20; $j<count($arreglo); $j++){
                  $descComp=$descComp." ".$arreglo[$j];
                }*/
                echo '<div class=col-md-4"><div class="card shadow-lg mx-2 my-3 rounded" style="width: 18rem;"><img src="'.$producto[4].'" class="card-img-top" alt="..."><div class="card-body"><h5 class="card-title">'.$producto[1].'</h5><span data-toggle="tooltip" title="'.$producto[2].'" data-placement="right"><p class="card-text">'.$producto[2].'...</span></p><a href="#" class="btn btn-primary">Agregar</a></div></div></div>';
                $desc="";
                $descComp="";
              }
  //            echo json_encode($_SESSION["productos"]);
              unset($_SESSION['productos']);
            }

            if(isset($_SESSION['productsFilt'])){
              foreach ($_SESSION['productsFilt'] as $producto) {
                //echo $producto[1];
                /*$arreglo=explode(" ",$producto[2]);
                for($i=0; $i<20; $i++){
                  $desc=$desc." ".$arreglo[$i];
                }
                for($j=20; $j<count($arreglo); $j++){
                  $descComp=$descComp." ".$arreglo[$j];
                }*/
                echo '<div class=col-md-4"><div class="card shadow-lg mx-2 my-3 rounded" style="width: 18rem;"><img src="'.$producto[4].'" class="card-img-top" alt="..."><div class="card-body"><h5 class="card-title">'.$producto[1].'</h5><span data-toggle="tooltip" title="'.$producto[2].'" data-placement="right"><p class="card-text">'.$producto[2].'...</span></p><a href="#" class="btn btn-primary">Agregar</a></div></div></div>';
                $desc="";
                $descComp="";
              }
  //            echo json_encode($_SESSION["productos"]);
              unset($_SESSION['productsFilt']);
            }

            if(isset($_SESSION['flash'])){
              echo '<div class="mx-5 text-light"><p class="h5 text-center">'.$_SESSION['flash'].'</p></div>';
              unset($_SESSION['flash']);
            }
          ?>
      </div>
    </div>
    <script src="https://kit.fontawesome.com/4c3daebd76.js" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script type="text/javascript">
    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    })
    </script>
  </body>
</html>
