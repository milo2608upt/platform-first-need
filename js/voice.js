var productos = [];
var pro ='';
var f = new Date();
var msg=document.getElementById('chat');
  if(annyang){
    comandos = {
      'hola *saludo': function(saludo){
        var sal=saludo;
        msg.innerHTML =
        `
        <div class="out-chats">
          <div class="out-chats-msg">
            <p>`+sal+` Bienvenido que necesitas</p>
            <span class="time">`+f.getHours()+`:`+f.getMinutes()+` | `+f.getDate() + "/" + (f.getMonth() +1) +`</span>
          </div>
          <div class="out-chats-img">
            <img src="../../PlataformaFirstNeed/Images/response.jpg" class="img-chat-response">
          </div>
        </div>
        `;
        msg.scrollTop =msg.scrollHeight;
        annyang.addCommands(comands_compra);
      }
    }
    comando_marca = {
      'dame un *marca': function(marca){
        productos.push({
          name: pro,
          marca: marca
        })
        console.log(productos);
        annyang.removeCommands(comando_marca);
      }
    }
    comands_compra = {
      'estoy listo': function(){
        annyang.resume();
        console.log("Estamos escuchano");
      },
      'necesito *producto': function(producto){ 
        fetch('?controller=producto&action=look_brand',{
          method: 'POST',
          body: new URLSearchParams('producto=' + producto)
        })
        .then(data => data.json())
        .then(data => {
          data.forEach(marcas => {
            marcas.forEach(marca => {
              pro = producto;
              msg.innerHTML += 
              `
              <div class="out-chats">
                <div class="out-chats-msg">
                  <p>`+marca+`</p>
                  <span class="time">11:01 PM | June 20</span>
                </div>
                <div class="out-chats-img">
                  <img src="../../PlataformaFirstNeed/Images/response.jpg" class="img-chat-response">
                </div>
              </div>
              `;
              annyang.addCommands(comando_marca);
            })
          })
          /**/
        })
      },
      'mostrar productos': function(){
        fetch('?controller=producto&action=see')
        .then(data => data.json())
        .then(data=>{
          console.log(data);
          data.forEach(producto => {
              msg.innerHTML +=
              `
                <div class="out-chats">
                    <div class="out-chats-msg">
                      <p class="text-uppercase">`+producto[1]+`<br>`+producto[5]+`</p>
                      <span class="time">11:01 PM | June 20</span>
                    </div>
                    <div class="out-chats-img">
                      <img src="`+producto[4]+`" class="img-chat-response">
                    </div>
                  </div>
              `;
              msg.scrollTop =msg.scrollHeight;
          });
        })
      },
      'mostrar carrito': function(){
        if(productos.length == 0){
            msg.innerHTML += 
            `
            <div class="out-chats">
            <div class="out-chats-msg">
            <p>Tu carrito esta vacio<br><i class="fas fa-lightbulb"></i> Agrega algun producto</p>
            <span class="time">11:01 PM | June 20</span>
            </div>
            <div class="out-chats-img">
            <img src="../../PlataformaFirstNeed/Images/response.jpg" class="img-chat-response">
            </div>
        </div>
            `;
            msg.scrollTop =msg.scrollHeight;
          
        }else{
        productos.forEach(producto => {
            msg.innerHTML +=
            `
            <div class="out-chats">
            <div class="out-chats-msg">
            <p><i class="fas fa-shopping-cart"></i>: `+producto.name+` `+producto.marca+`<br><i class="fas fa-hashtag"></i>: `+producto.cantidad+` `+producto.presentacion+`
            <br><i class="fas fa-coins"></i>: $ `+producto.costo+`</p>
            <span class="time">11:01 PM | June 20</span>
            </div>
            <div class="out-chats-img">
            <img src="../../PlataformaFirstNeed/Images/response.jpg" class="img-chat-response">
            </div>
            `;
            msg.scrollTop =msg.scrollHeight;
        });
        }
      },
      'eliminar *producto *marca': function(producto,marca){
        console.log(producto+" "+marca);
      }
    }

    annyang.addCommands(comandos);
    annyang.setLanguage("es-MX");
    annyang.start();
  }