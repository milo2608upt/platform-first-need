<?php
    if(isset($_GET["controller"])&&isset($_GET["action"])){
        header('Access-Control-Allow-Origin: *');
        $controlador=$_GET["controller"]."Controller";
        $accion=$_GET["action"];
        require ("Controllers/".$controlador.".php");
        $instancia=new $controlador();

        $instancia->{$accion}();
    }else{
        echo "No existen parametros";
    }
?>
