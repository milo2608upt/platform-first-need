<?php
namespace PlataformaFirstNeed\Models;

class DetalleOrden extends Conexion
{
    public $id;
    public $id_order;
    public $id_product;
    public $cantidad;
    public $precio;
    
    public function create()
    {
        $pre=mysqli_prepare($this->conn, "INSERT INTO detalle_orden (id_order, id_product,cantidad,precio) VALUES (?, ?, ?,?)");
        $pre->bind_param("iiii", $this->id_order, $this->id_product, $this->cantidad, $this->precio);
        $pre->execute();
        $pre_=mysqli_prepare($this->conn, "SELECT LAST_INSERT_ID() id");
        $pre_->execute();
        $result=$pre_->get_result();
        $this->id=$result->fetch_assoc()['id'];
        var_dump($result);
        if ($this->id!=null) {
            return json_encode(["estado"=>true,"detalle"=>$this->id]);
        } else {
            return json_encode(["estado"=>false]);
        }
    }
    public function update()
    {
        $pre=mysqli_prepare($this->conn, "UPDATE detalle_orden SET id_order=?, id_product=?, cantidad=?, precio=? where id=?");
        $pre->bind_param("iiiii", $this->id_order, $this->id_product, $this->cantidad, $this->precio, $this->id);
        $status=$pre->execute();
        if ($status) {
            return json_encode(["estado"=>true,"detalle"=>$this]);
        } else {
            return json_encode(["estado"=>false]);
        }
    }
    public function delete()
    {
        $pre=mysqli_prepare($this->conn, "DELETE from detalle_orden where id=?");
        $pre->bind_param("i", $this->id);
        $status=$pre->execute();
        if ($status) {
            return json_encode(["estado"=>true]);
        } else {
            return json_encode(["estado"=>false]);
        }
    }
    public function find()
    {
        $pre=mysqli_prepare($this->conn, "SELECT * from detalle_orden where id=?");
        $pre->bind_param("i", $this->id);
        $status=$pre->execute();
        $result=$pre->get_result();
        $usuario=$result->fetch_object(DetalleOrden::class);
        if ($status) {
            return json_encode(["estado"=>true,"detalle"=>$usuario]);
        } else {
            return json_encode(["estado"=>false]);
        }
    }
}
