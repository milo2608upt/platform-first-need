<?php

namespace PlataformaFirstNeed\Models;

class Repartidor extends Conexion
{
    public $id;
    public $nombre;
    public $contacto;
    public $imagen;
    public function insert()
    {
        $pre = mysqli_prepare($this->conn, "INSERT INTO repartidor (nombre, contacto, imagen) VALUES (?, ?, ?)");
        $pre->bind_param("sss", $this->nombre, $this->contacto, $this->imagen);
        $pre->execute();
        $pre_ = mysqli_prepare($this->conn, "SELECT LAST_INSERT_ID() id");
        $pre_->execute();
        $result = $pre_->get_result();
        $this->id = $result->fetch_assoc()['id'];
        if ($this->id > 0) {
            return true;
        } else {
            return false;
        }
    }
    public function update()
    {
        $pre = mysqli_prepare($this->conn, "UPDATE repartidor SET nombre=? where id=?");
        $pre->bind_param("si", $this->nombre, $this->idusuario);
        $estado = $pre->execute();
        if ($estado) {
            return json_encode(["estado" => true, "detalle" => $this->id]);
        } else {
            return json_encode(["estado" => false]);
        }
    }
    public function delete()
    {

        $pre = mysqli_prepare($this->conn, "DELETE from repartidor where id=?");
        $pre->bind_param("i", $this->id);
        $status = $pre->execute();
        if ($status) {
            return json_encode(["estado" => true]);
        } else {
            return json_encode(["estado" => false]);
        }
    }
    public function find()
    {
        $pre=mysqli_prepare($this->conn, "SELECT * from repartidor where id=?");
        $pre->bind_param("i", $this->id);
        $status=$pre->execute();
        $result=$pre->get_result();
        $result->fetch_Object($this);
        if ($status) {
            return json_encode(["estado"=>true]);
        } else {
            return json_encode(["estado"=>false]);
        }
}
