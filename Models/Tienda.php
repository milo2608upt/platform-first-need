<?php
namespace PlataformaFirstNeed\Models;

class Tienda extends Conexion
{
    public $id;
    public $dirrecion;
    public $encargado;
    public $latitud;
    public $longitud;
    public $contacto;
    public function insert()
    {
        $pre=mysqli_prepare($this->conn, "INSERT INTO tiendas ( direccion,encargado,latitud,longitud,contacto) VALUES (?,?,?,?,?)");
        $pre->bind_param("iiiii",$this->dirrecion,$this->encargado,$this->latitud,$this->longitud,$this->contacto);
        $pre->execute();
        $pre_=mysqli_prepare($this->conn, "SELECT LAST_INSERT_ID() id");
        $pre_->execute();
        $result=$pre_->get_result();
        $this->id=$result->fetch_assoc()['id'];
        if ($this->id>0) {
            return true;
        } else {
            return false;
        }
    }
    
    public function select()
    {
        $pre=mysqli_prepare($this->conn, "SELECT * from tiendas  WHERE id=?");
        $pre->bind_param("s", $this->id);
        $pre->execute();
        $result=$pre->get_result();
        $this->id=$result->fetch_assoc()['id'];
        if ($this->token!="") {

            return $this->token;
        } else {
            return false;
        }
    }
    public function Update()
    {
        $pre=mysqli_prepare($this->conn,"UPDATE tiendas SET  direccion=?, encargado=?, latitud=?, longitud=?,contacto=? WHERE id=?");
        $pre->bind_param("iiiiii", $this->dirrecion,$this->encargado, $this->latitud, $this->longitud, $this->contacto, $this->id);
        $estado= $pre->execute();
        if ($estado) {
            return json_encode(["estado"=>true,"detalle"=>$this->id]);
        } else {
            return json_encode(["estado"=>false]);
        }
    }
    static function delete($id){
        $me=new Conexion();
        $pre=mysqli_prepare($this->conn,"DELETE FROM tiendas WHERE id=?");
        $pre->bind_param("i",$id);
        $pre->execute();
        $res=$pre->get_result();
        return true;

    }
}