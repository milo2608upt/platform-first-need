<?php
namespace PlataformaFirstNeed\Models;

class Usuario extends Conexion
{
    public $id;
    public $usuario;
    public $password;
    public $pass;
    public $passhash;
    public $tipo;
    public $nombre;
    public $apellidos;
    public $fecha;
    public $status;

    public static function validate($usuario, $password)
    {
        $me=new Conexion();
        $pre=mysqli_prepare($me->conn, "SELECT * FROM Usuarios WHERE Usuario=?");
        $pre->bind_param("s", $usuario);
        $pre->execute();
        $result=$pre->get_result();
        $pass=$result->fetch_assoc();
        $passhash=$pass['Password'];
        if (password_verify($password, $passhash)) {
            return $pass;
        } else {
            return null;
        }
    }
    public function insert()
    {
        $pre=mysqli_prepare($this->conn, "INSERT INTO usuarios (Usuario, Password, Tipo, Nombre, Apellidos, Fecha_Nac, Status) VALUES (?, ?, ?, ?, ?, ?, ?)");
        $pre->bind_param("ssisssi", $this->usuario, $this->password, $this->tipo, $this->nombre, $this->apellidos, $this->fecha, $this->status);
        $pre->execute();
        $pre_=mysqli_prepare($this->conn, "SELECT LAST_INSERT_ID() id");
        $pre_->execute();
        $result=$pre_->get_result();
        $this->id=$result->fetch_assoc()['id'];
        if ($this->id>0) {
            return true;
        } else {
            return false;
        }
    }

    public function activate()
    {
        $me=new Conexion();
        $pre=mysqli_prepare($me->conn, "UPDATE usuarios SET Status = '1' WHERE Usuario = ?");
        $pre->bind_param("s", $this->usuario);
        $pre->execute();
        $result=$pre->get_result();
        return true;
    }
}
