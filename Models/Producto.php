<?php
namespace PlataformaFirstNeed\Models;

class Producto extends Conexion{
  public $id;
  public $nombre;
  public $descripcion;
  public $unidad;
  public $imagen;
  function __construct(){

  }

  static function show(){
    $me=new Conexion();
    $pre=mysqli_prepare($me->conn,"SELECT * FROM productos");
    $pre->execute();
    $result=$pre->get_result();
    return $result->fetch_all();
  }

  static function seeker_brand($product){
    $me=new Conexion();
    $pre = mysqli_prepare($me->conn,"SELECT marca FROM productos WHERE producto=?");
    $pre->bind_param("s",$product);
    $pre->execute();
    $result=$pre->get_result();
    return $result->fetch_all();
  }
}
?>
