<?php
namespace PlataformaFirstNeed\Models;

class RecuperarContrasenia extends Conexion
{
    public $Usuario;
    public $idusuario;
    public $token;
    public $contrasenia;
    public function insert()
    {
        $pre=mysqli_prepare($this->conn, "INSERT INTO recuperarcontrasenia (idusuario, token) VALUES (?, ?)");
        $pre->bind_param("is", $this->idusuario, $this->token);
        $pre->execute();
        $pre_=mysqli_prepare($this->conn, "SELECT LAST_INSERT_ID() id");
        $pre_->execute();
        $result=$pre_->get_result();
        $this->id=$result->fetch_assoc()['id'];
        if ($this->id>0) {
            return true;
        } else {
            return false;
        }
    }
    public function selectus()
    {
        $pre=mysqli_prepare($this->conn, "SELECT id from usuarios WHERE Usuario=?");
        $pre->bind_param("s", $this->Usuario);
        $pre->execute();
        $result=$pre->get_result();
        $this->id=$result->fetch_assoc()['id'];
        return $this->id;
    }
    public function selecttoken()
    {
        $pre=mysqli_prepare($this->conn, "SELECT * from recuperarcontrasenia WHERE idusuario=?");
        $pre->bind_param("s", $this->idusuario);
        $pre->execute();
        $result=$pre->get_result();
        $this->token=$result->fetch_assoc()['token'];
        if ($this->token!="") {
            return $this->token;
        } else {
            return false;
        }
    }
    public function cambiarpass()
    {
        $pre=mysqli_prepare($this->conn, "UPDATE usuarios SET Password=? where id=?");
        $pre->bind_param("si", $this->password, $this->idusuario);
        $estado= $pre->execute();
        if ($estado) {
            return json_encode(["estado"=>true,"detalle"=>$this->id]);
        } else {
            return json_encode(["estado"=>false]);
        }
        
        
      
        /* $pre_=mysqli_prepare($this->conn, "SELECT LAST_INSERT_ID() id");
        $pre_->execute();
        $estatus */
    }
}
