<?php
namespace PlataformaFirstNeed\Models;
use PlataformaFirstNeed\Models\Conexion;

class Carrito extends Conexion
{
    public $id;
    public $idusuario;
    public $idproducto;
    public $cantidad;
    public function insert()
    {
        $pre=mysqli_prepare($this->conn, "INSERT INTO carrito (id_user, id_product, cantidad) VALUES (?, ?, ?)");
        $pre->bind_param("iii", $this->idusuario, $this->token);
        $pre->execute();
        $pre_=mysqli_prepare($this->conn, "SELECT LAST_INSERT_ID() id");
        $pre_->execute();
        $result=$pre_->get_result();
        $this->id=$result->fetch_assoc()['id'];
        if ($this->id>0) {
            return true;
        } else {
            return false;
        }
    }
}
?>