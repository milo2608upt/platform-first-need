<?php
namespace PlataformaFirstNeed\Models;

class MetodoPagoController extends Conexion
{
    public $id;
    public $nombre;
    public function insert()
    {
        $pre=mysqli_prepare($this->conn, "INSERT INTO metodopago (id, nombre) VALUES (?, ?)");
        $pre->bind_param("is", $this->id $this->nombre);
        $pre->execute();
        $pre_=mysqli_prepare($this->conn, "SELECT LAST_INSERT_ID() id");
        $pre_->execute();
        $result=$pre_->get_result();
        $this->id=$result->fetch_assoc()['id'];
        if ($this->id>0) {
            return true;
        } else {
            return false;
        }
    }
    public function select()
    {
        $pre=mysqli_prepare($this->conn, "SELECT id from metodpago WHERE nombre=?");
        $pre->bind_param("s", $this->nombre);
        $pre->execute();
        $result=$pre->get_result();
        $this->id=$result->fetch_assoc()['id'];
        return $this->id;
    }
    public function selectF()
    {
        $pre=mysqli_prepare($this->conn, "SELECT * from metodopago WHERE id=?");
        $pre->bind_param("s", $this->id);
        $pre->execute();
        $result=$pre->get_result();
        $this->id=$result->fetch_assoc()['id'];
        if ($this->token!="") {

            return $this->token;
        } else {
            return false;
        }
    }
    public function Updade()
    {
        $pre=mysqli_prepare($this->conn, "UPDATE nombre where id=?");
        $pre->bind_param("si", $this->password, $this->idusuario);
        $estado= $pre->execute();
        if ($estado) {
            return json_encode(["estado"=>true,"detalle"=>$this->id]);
        } else {
            return json_encode(["estado"=>false]);
        }
    }
}