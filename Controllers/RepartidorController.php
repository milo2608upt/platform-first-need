<?php
session_start();

use PlataformaFirstNeed\Models\Repartidor;

include 'Models/Conexion.php';
include 'Models/Repartidor.php';

class RepartidorController
{
    public function insertar()
    {
        $repartidor= new Repartidor();
        $repartidor->nombre=$_POST['nombre'];
        $repartidor->contacto=$_POST['contacto'];
        $repartidor->imagen=$_POST['imagen'];
        $repartidor->insert();
    }
    public function actualizar()
    {
        $repartidor= new Repartidor();
        $repartidor->id=$_POST["id"];
        $repartidor->nombre=$_POST['nombre'];
        $repartidor->contacto=$_POST['contacto'];
        $repartidor->imagen=$_POST['imagen'];
        $repartidor->update();
    }
    public function eliminar()
    {
        $repartidor= new Repartidor();
        $repartidor->id=$_POST["id"];
        $repartidor->delete();
       
    }
    public function buscarid()
    {
        

    }
    //Hasta la proxima
}
