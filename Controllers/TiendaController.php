<?php
session_start();

use PlataformaFirstNeed\Models\Tienda;

include 'Models/Conexion.php';
include 'Models/Tienda.php';

class TiendaController
{
    public function insertar()
    {
        $tienda= new Tienda();
        $tienda->dirrecion=$_POST['dirrecion'];
        $tienda->encargado=$_POST['encargado'];
        $tienda->latitud=$_POST['latitud'];
        $tienda->longitud=$_POST['longitud'];
        $tienda->contacto=$_POST['contacto'];
        $tienda->insert();
    }
    public function actualizar()
    {
       
        $tienda= new Tienda();
        $tienda->dirrecion=$_POST['dirrecion'];
        $tienda->encargado=$_POST['encargado'];
        $tienda->latitud=$_POST['latitud'];
        $tienda->longitud=$_POST['longitud'];
        $tienda->contacto=$_POST['contacto'];
        $tienda->Update();
    }
   
    function eliminar(){
        $id=$_POST["id"];
        
        if($_POST["id"]==""){
            echo json_encode(["estatus"=>"error","msj"=>"error"]);
        }else{
            $res=Tienda::delete($id);
                echo json_encode(["estatus"=>"success","msj"=>$res]);
        }
    }
    function buscar(){
        $id=$_POST["id"];
        if($id!=""){
            $res=Tienda::select($id);
            if($res["id"]!=$id){
                echo "no existe usuario";
            
            }else{
                echo json_encode($res);
            }
        }else{
            echo json_encode(["estatus"=>"success","msj"=>"error"]);
        }
    }
    //Hasta la proxima
}
