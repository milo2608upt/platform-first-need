<?php
session_start();

use PlataformaFirstNeed\Models\Carrito;

include 'Models/Conexion.php';
include 'Models/Carrito.php';

class CarritoController
{
    function insertarproducto()
    {
        $carrito = new Carrito();
        $carrito->idusuario = session_id($_POST['session_id']);
        $carrito->producto = $_SESSION['productos'];
        $carrito->cantidad = $_POST['cantidad'];
        $carrito->insert();
    }
}
