<?php
session_start();

use PlataformaFirstNeed\Models\DetalleOrden;

include 'Models/Conexion.php';
include 'Models/DetalleOrden.php';

class DetalleOrdenController
{
    public function insertar()
    {
        $detalle= new DetalleOrden();
        $detalle->id_order=$_POST["id_order"];
        $detalle->id_product=$_POST["id_product"];
        $detalle->cantidad=$_POST["cantidad"];
        $detalle->precio=$_POST["precio"];
        $res=$detalle->create();
        echo $res;
    }
    public function actualizar()
    {
        $detalle= new DetalleOrden();
        $detalle->id=$_POST["id"];
        $detalle->id_order=$_POST["id_order"];
        $detalle->id_product=$_POST["id_product"];
        $detalle->cantidad=$_POST["cantidad"];
        $detalle->precio=$_POST["precio"];
        $res=$detalle->update();
        echo $res;
    }
    public function eliminar()
    {
        $detalle= new DetalleOrden();
        $detalle->id=$_POST["id"];
        $res=$detalle->delete();
        echo $res;
    }
    public function encontrar()
    {
        $detalle= new DetalleOrden();
        $detalle->id=$_POST["id"];
        $res=$detalle->find();
        echo $res;
    }
    
}

