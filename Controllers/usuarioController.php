<?php
session_start();
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PlataformaFirstNeed\Models\Usuario;

require 'PHPMailer/PHPMailer/src/Exception.php';
require 'PHPMailer/PHPMailer/src/PHPMailer.php';
require 'PHPMailer/PHPMailer/src/SMTP.php';

include 'Models/Conexion.php';
include 'Models/Usuario.php';

class usuarioController
{
    public function login()
    {
        require_once 'Views/Login.php';
    }

    public function homeUser()
    {
        require_once 'Views/HomeUser.php';
    }

    public function homeSender()
    {
        require_once 'Views/HomeSender.php';
    }

    public function registry()
    {
        require_once 'Views/Registry.php';
    }

    public function enviar()
    {
        $mail = new PHPMailer(true);

        try {
            //Server settings                    // Enable verbose debug output
        $mail->isSMTP();                                            // Send using SMTP
        $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
        $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
        $mail->Username   = 'alfredomendez60@gmail.com';                     // SMTP username
        $mail->Password   = 'lokillo101100';                               // SMTP password
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
        $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

        //Recipients
            $mail->setFrom('emilio.andere.l@gmail.com', 'Mailer');
            $mail->addAddress($_GET['email'], $_GET['nombre']);               // Name is optional
            //$mail->addReplyTo('info@example.com', 'Information');
            //$mail->addCC('cc@example.com');
            //$mail->addBCC('bcc@example.com');

            // Attachments
            //  $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
            //  $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

            // Content
          $mail->isHTML(true);                                  // Set email format to HTML
          $mail->Subject = 'Activacion';
            $mail->Body    = '<a class="btn" style="text-decoration: none; padding: 10px; font-weight: 600; font-size: 20px; color: #ffffff; background-color: #1883ba; border-radius: 6px; border: 2px solid #0016b0;" href="localhost/PlataformaFirstNeed/?controller=usuario&action=account&data='.$_GET['email'].'">Activar</a>';
            //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

            $mail->send();
            header("Location: /?controller=usuario&action=login");
        } catch (Exception $e) {
            echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
        }
    }

    public function validation()
    {
        $user=$_POST["username"];
        $pass=$_POST["password"];
        $usuario =Usuario::validate($user, $pass);
        if ($usuario!=null) {
            if ($usuario['Status']==0) {
                $_SESSION['flash']="Aun no has activado tu cuenta";
                header('location: ?controller=usuario&action=login');
            } else {
                if ($usuario['Tipo']=='1') {
                    header('location: ?controller=usuario&action=homeUser');
                } else {
                    header('location: ?controller=usuario&action=homeSender');
                }
            }
        } else {
            $_SESSION["status"]="Error en tus datos de inicio de sesion";
            header('location: ?controller=usuario&action=login');
        }
    }

    public function signup()
    {
        $usuario=new Usuario();
        $usuario->nombre=$_POST['nombre'];
        $usuario->apellidos=$_POST['apellidos'];
        $dia=$_POST['dia'];
        $mes=$_POST['mes'];
        $año=$_POST['año'];
        $usuario->fecha=$año."-".$mes."-".$dia;
        $usuario->usuario=$_POST['email'];
        $usuario->password=password_hash($_POST['password'], PASSWORD_BCRYPT);
        $usuario->tipo=$_POST['tipo'];
        $usuario->status=0;
        if ($usuario->tipo!=0) {
            $result=$usuario->insert();
            $_SESSION['flash']="Activa tu cuenta para iniciar sesion";
            if ($result==true) {
                header("Location: ?controller=usuario&action=enviar&email=".$usuario->usuario."&nombre=".$usuario->nombre);
            } else {
                header("Location: ?controller=usuario&action=registry");
            }
        } else {
            header("Location: ?controller=usuario&action=registry");
        }
        //echo $usuario->nombre.$usuario->apellidos.$usuario->fecha.$usuario->usuario.$usuario->password.$usuario->tipo;
    }

    public function account()
    {
        $usuario=new Usuario();
        $usuario->usuario=$_GET['data'];
        $res=$usuario->activate();
        if ($res==true) {
            header("Location: ?controller=usuario&action=login");
        } else {
            header("Location: ?controller=usuario&action=registry");
        }
    }
}
